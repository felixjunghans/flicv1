import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flic_v1/flic_v1.dart';

void main() {
  const MethodChannel channel = MethodChannel('flic_v1');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await FlicV1.platformVersion, '42');
  });
}
