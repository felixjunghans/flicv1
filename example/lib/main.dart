import 'dart:async';

import 'package:flic_v1/flic_v1.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FlicV1 _flicV1 = FlicV1();

  @override
  void initState() {
    super.initState();

    _flicV1.onFlicButtonClick.listen((FlicButtonClick flicButtonClick) {
      print(flicButtonClick);
    });
  }

  Future<void> startScan() async {
    try {
      _flicV1.startScan();
    } catch (e) {
      print(e);
    }
  }

  Future<void> forgetButton(String identifier, String version) async {
    try {
      _flicV1.forgetButton(identifier, version);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: StreamBuilder<List<FlicButton>>(
            stream: _flicV1.connectedButtons,
            initialData: [],
            builder: (context, snapshot) => Column(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: snapshot.data
                            .map((button) => Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () =>
                                            button.setButtonName("HalloWelt"),
                                        child: Column(
                                          children: <Widget>[
                                            Text(
                                              button.name,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Text(
                                              button.isReady.toString(),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Text(
                                              button.connectionState.toString(),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Text(
                                              button.batteryStatus.toString(),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            if (button.buttonDown)
                                              Container(
                                                width: 10.0,
                                                height: 10.0,
                                                color: Colors.green,
                                              )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: RaisedButton(
                                        onPressed: () => forgetButton(
                                            button.identifier, button.version),
                                        child: Text("Remove Button"),
                                      ),
                                    ),
                                  ],
                                ))
                            .toList(),
                      ),
                    ),
                    StreamBuilder<bool>(
                        initialData: false,
                        stream: _flicV1.isScanning,
                        builder: (context, snapshot) {
                          if (snapshot.data == false) {
                            return RaisedButton(
                              onPressed: startScan,
                              child: Text("Start Scan"),
                            );
                          }
                          return CircularProgressIndicator();
                        }),
                  ],
                )),
      ),
    );
  }
}
