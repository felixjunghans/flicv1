package de.sdpdp.logbuch.flic_v1;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.UiThread;

import java.util.HashMap;

import io.flutter.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** FlicV1Plugin */
public class FlicV1Plugin implements FlutterPlugin, MethodCallHandler, ButtonCallBack {
  FlicHandler flicHandler;
  FlicHandlerV2 flicHandlerV2;
  HashMap buttonsv1 = new HashMap();
  HashMap buttonsv2 = new HashMap();
  MethodChannel channel;

  public FlicV1Plugin() {}

  FlicV1Plugin(MethodChannel channel, Context context) {
    this.channel = channel;
    Log.d("initialize", "FELIXIMUS initialize...");
    flicHandler = new FlicHandler(context, channel, this::updateButtons);
    flicHandlerV2 = new FlicHandlerV2(context, channel, this::updateButtons);
  }

  public static class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      // let empty to support older versions of logbuch
    }
  }

  public static class UpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      // let empty to support older versions of logbuch
    }
  }

  @Override
  public void onAttachedToEngine(FlutterPluginBinding flutterPluginBinding) {
    MethodChannel channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flic_v1");
    channel.setMethodCallHandler(new FlicV1Plugin(channel, flutterPluginBinding.getApplicationContext()));
  }

  @Override
  public void onMethodCall(MethodCall call, Result result) {
    Log.d("methodCall", "FELIXIMUS call Method " + call.method + " with " + (call.arguments != null ? call.arguments.toString() : ""));
    switch (call.method) {
      case "startScan":
        flicHandler.stopScan();
        flicHandlerV2.stopScan();

        flicHandler.startScan();
        flicHandlerV2.startScan();
        break;
      case "forgetButton":
        HashMap args = (HashMap) call.arguments;

        if(args != null) {
          String identifier = args.get("identifier").toString();
          String version = args.get("version").toString();

          if(version.equals("1")) {
            flicHandler.forgetButton(identifier);
          } else {
            flicHandlerV2.forgetButton(identifier);
          }

        }
        break;
      case "indicateButtonLED":
        flicHandler.indicateButtonLED(call.arguments.toString());
        break;
      case "connect":
        HashMap connectArgs = (HashMap) call.arguments;

        if(connectArgs != null) {
          String identifier = connectArgs.get("identifier").toString();
          String version = connectArgs.get("version").toString();

          if(version.equals("1")) {
            flicHandler.connect(identifier);
          } else {
            flicHandler.connect(identifier);
          }

        }
        break;
      case "disconnect":
        HashMap disconnectArgs = (HashMap) call.arguments;

        if(disconnectArgs != null) {
          String identifier = disconnectArgs.get("identifier").toString();
          String version = disconnectArgs.get("version").toString();

          if(version.equals("1")) {
            flicHandler.disconnect(identifier);
          } else {
            flicHandler.disconnect(identifier);
          }

        }
        break;
      case "startService":
        flicHandler.startForgroundService();
        break;
      case "stopService":
        flicHandler.stopForgroundService();
        break;
      case "getBluetoothState":
        flicHandler.getBluetoothState();
        break;
      case "getKnownButtons":
        flicHandler.getKnownButtons();
        flicHandlerV2.getKnownButtons();
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  @UiThread
  public void updateButtons(HashMap buttons, boolean isV1) {
    if(isV1) {
      buttonsv1 = buttons;
    } else {
      buttonsv2 = buttons;
    }
    sendKnownButtons();
  }

  @UiThread
  public void sendKnownButtons() {
    HashMap buttons = new HashMap();
    buttons.putAll(buttonsv1);
    buttons.putAll(buttonsv2);
    channel.invokeMethod("connectedButtons", buttons);
  }

  @Override
  public void onDetachedFromEngine(FlutterPluginBinding binding) {
    if(flicHandler != null) {
      flicHandler.stopScan();
      flicHandler.destroy();
    }
    if(flicHandlerV2 != null) {
      flicHandlerV2.stopScan();
      flicHandlerV2.destroy();
    }
  }
}

interface ButtonCallBack {
  void updateButtons(HashMap buttons, boolean isV1);
}