package de.sdpdp.logbuch.flic_v1;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.graphics.BitmapFactory;

import androidx.annotation.Nullable;

public class FlicBackgroundService extends Service {
    private static final int SERVICE_NOTIFICATION_ID = 1909;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Intent notificationIntent = new Intent(this, FlicV1Plugin.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setContentTitle("LogBuch")
                .setContentText("Wird im Hintergrund ausgeführt...")
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), this.getResources().getIdentifier("de_sdpdp_logbuch_service_icon", "drawable", this.getPackageName())))
                .setContentIntent(contentIntent)
                .setOngoing(true)
                .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startMyOwnForeground();
        } else {
            startForeground(SERVICE_NOTIFICATION_ID, notification);
        }
    }

    private void startMyOwnForeground(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            Intent notificationIntent = new Intent(this, FlicV1Plugin.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            String NOTIFICATION_CHANNEL_ID = "de.sdpdp.logbuch";
            String channelName = "LogBuch";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            Notification notification = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID).setOngoing(true)
                    .setContentTitle("LogBuch")
                    .setContentText("Wird im Hintergrund ausgeführt...")
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), this.getResources().getIdentifier("de_sdpdp_logbuch_service_icon", "drawable", this.getPackageName())))
                    .setContentIntent(contentIntent)
                    .setOngoing(true)
                    .build();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                startForeground(SERVICE_NOTIFICATION_ID, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION | ServiceInfo.FOREGROUND_SERVICE_TYPE_MICROPHONE);
            } else {
                startForeground(SERVICE_NOTIFICATION_ID, notification);
            }
        }
    }
}
