package de.sdpdp.logbuch.flic_v1;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.flic.flic2libandroid.BatteryLevel;
import io.flic.flic2libandroid.Flic2Button;
import io.flic.flic2libandroid.Flic2ButtonListener;
import io.flic.flic2libandroid.Flic2Manager;
import io.flic.flic2libandroid.Flic2ScanCallback;
import io.flutter.Log;
import io.flutter.plugin.common.MethodChannel;

import static io.flic.flic2libandroid.Flic2Button.CONNECTION_STATE_CONNECTED_READY;

public class FlicHandlerV2 {
    private MethodChannel _channel;
    private boolean _isScanning = false;
    private HashMap<String, Flic2ButtonListener> listeners = new HashMap();
    private Boolean serviceStarted = false;
    private Context _context;
    private ButtonCallBack _buttonCallBack;

    public FlicHandlerV2(Context context, MethodChannel channel, ButtonCallBack buttonCallBack) {
        Flic2Manager.init(context, new Handler());
        _channel = channel;
        _context = context;
        _buttonCallBack = buttonCallBack;
        restoreButtonState();
        updateKnownButtons();
    }

    private Handler uiThreadHandler = new Handler(Looper.getMainLooper());

    private void restoreButtonState() {
       List<Flic2Button> buttons = Flic2Manager.getInstance().getButtons();

       for (Flic2Button button : Flic2Manager.getInstance().getButtons()) {
             button.connect();
             button.setAutoDisconnectTime(511);
             addButtonListener(button);
       }

       new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
              getKnownButtons();
              invokeMethod("didRestore", "");
           }
       }, 5000);
    }

    private void updateKnownButtons() {
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                getKnownButtons();
            }
        },0,30000);
    }

    public void startScan() {
        Log.d("startscan", "FELIXIMUS Scanning...");
        isScanning(true);
        Flic2Manager.getInstance().startScan(new Flic2ScanCallback() {
            @Override
            public void onDiscoveredAlreadyPairedButton(Flic2Button button) {
                Log.d("DiscoveredInPrivateMode", "FELIXIMUS Found a private button. Hold it down for 7 seconds to make it public." + " - " );
            }

            @Override
            public void onDiscovered(String bdAddr) {
                Log.d("DiscoveredInNONEPrivateMode", "FELIXIMUS Now connecting to... ." + " - " + bdAddr );
                invokeMethod("didDiscover", "");
            }

            @Override
            public void onConnected() {
                Log.d("ConnectionEstablished", "FELIXIMUS Connection established. Now verifying..." + " - " );
            }

            @Override
            public void onComplete(int result, int subCode, Flic2Button button) {
                if (result == Flic2ScanCallback.RESULT_SUCCESS) {
                    addButtonListener(button);
                    getKnownButtons();
                    isScanning(false);
                } else {
                    isScanning(false);
                    invokeMethod("connectingFailed", "");
                }
            }
        });
    }

    private void addButtonListener(Flic2Button button) {
        Flic2ButtonListener listener = new Flic2ButtonListener() {
            @Override
            public void onConnect(Flic2Button button) {
                Log.d("connect BUTTON", "FELIXIMUS BUTTON CONNECTED");
                getKnownButtons();
            }

            @Override
            public void onReady(Flic2Button button, long timestamp) {
                Log.d("connect BUTTON", "FELIXIMUS BUTTON READY");
                getKnownButtons();
            }

            @Override
            public void onDisconnect(Flic2Button button) {
                Log.d("connect BUTTON", "FELIXIMUS BUTTON DISCONNECTED");
                getKnownButtons();
            }

            @Override
            public void onButtonUpOrDown(Flic2Button button, boolean wasQueued, boolean lastQueued, long timestamp, boolean isUp, boolean isDown) {
                if (!(wasQueued && button.getReadyTimestamp() - timestamp > 3000)) {
                    if (isUp) {
                        invokeMethod("didReceiveButtonUp", button.getUuid());
                    } else if (isDown) {
                        invokeMethod("didReceiveButtonDown", button.getUuid());
                    }
                }
            }

            @Override
            public void onButtonSingleOrDoubleClickOrHold(Flic2Button button, boolean wasQueued, boolean lastQueued, long timestamp, boolean isSingleClick, boolean isDoubleClick, boolean isHold) {
                Log.d("Button click", "FELIXIMUS BUTTON CLICK");
                if(!(wasQueued && button.getReadyTimestamp() - timestamp > 3000)) {
                    if(isSingleClick) {
                        invokeMethod("didReceiveButtonClick", button.getUuid());
                    } else if(isDoubleClick) {
                        invokeMethod("didReceiveButtonDoubleClick", button.getUuid());
                    } else if (isHold) {
                        invokeMethod("didReceiveButtonHold", button.getUuid());
                    }
                }
            }
        };

        listeners.put(button.getUuid(), listener);
        try {
            button.removeListener(listener);
        } catch (Exception e) {}
        button.addListener(listener);
        startForgroundService();
    }

    private void invokeMethod(String method, Object content) {
        uiThreadHandler.post(() -> _channel.invokeMethod(method, content));
    }

    private void removeButtonListener(Flic2Button button) {
        button.removeListener(listeners.get(button.getUuid()));
        listeners.remove(button.getUuid());
    }

    public void stopScan() {
        Flic2Manager.getInstance().stopScan();
        isScanning(false);
    }

    public void connect(String identifier) {
        Flic2Button button = getButtonFromIdentifier(identifier);

        if(button != null) {
            button.connect();
            button.setAutoDisconnectTime(511);
        }
    }

    public void disconnect(String identifier) {
        Flic2Button button = getButtonFromIdentifier(identifier);

        if(button != null) {
            button.disconnectOrAbortPendingConnection();
        }
    }

    public Flic2Button getButtonFromIdentifier(String identifier) {
        Flic2Button button = null;

        List<Flic2Button> knownButtons = Flic2Manager.getInstance().getButtons();

        Log.d("knwon button size", Integer.toString(knownButtons.size()));

        for(int i = 0; i < knownButtons.size(); i++) {
           Log.d("button id", "FELIXIMUS button id: " + (knownButtons.get(i).getUuid()) + " " + identifier);
           if(knownButtons.get(i).getUuid().equals(identifier)) {
               button = knownButtons.get(i);
           }
        }

        return button;
    }

    public void forgetButton(String identifier) {
        Flic2Button button = getButtonFromIdentifier(identifier);

        if(button != null) {
            button.disconnectOrAbortPendingConnection();
           Flic2Manager.getInstance().forgetButton(button);
           removeButtonListener(button);
           if(listeners.size() < 1) {
               stopForgroundService();
           }
        }
        getKnownButtons();
    }

    public void indicateButtonLED(String identifier) {
            Log.d("BUTTON LED", "FELIXIMUS show LED for " + identifier);
    }

    public void getKnownButtons() {
        HashMap<String, HashMap> buttons = new HashMap<>();
        List<Flic2Button> knownButtons = Flic2Manager.getInstance().getButtons();

        for (Flic2Button button : Flic2Manager.getInstance().getButtons()) {
            HashMap newButton = new HashMap();
            int connectionState = 2;

            if(button.getConnectionState() == CONNECTION_STATE_CONNECTED_READY) {
                connectionState = 0;
            }

            BatteryLevel batteryLevel = button.getLastKnownBatteryLevel();

            newButton.put("identifier", button.getUuid());
            newButton.put("name", button.getSerialNumber());
            newButton.put("batteryStatus", 0);
            newButton.put("batteryVoltage", batteryLevel != null ? batteryLevel.getVoltage() : null);
            newButton.put("firmwareRevision", button.getFirmwareVersion());
            newButton.put("isReady", true);
            newButton.put("connectionState",  connectionState);
            newButton.put("version", "2");

            buttons.put(button.getUuid(), newButton);
        }
        sendKnownButtons(buttons);
    }

    public void isScanning(boolean isScanning) {
        _isScanning = isScanning;
        invokeMethod("onScanningStateChange", _isScanning);
    }

    public void sendKnownButtons(HashMap buttons) {
        uiThreadHandler.post(() -> _buttonCallBack.updateButtons(buttons, false));
    }

    private void startForgroundService() {
        if(!serviceStarted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                _context.startForegroundService(new Intent(_context, FlicBackgroundService.class));
            } else {
                _context.startService(new Intent(_context, FlicBackgroundService.class));
            }
            serviceStarted = true;
        }
    }

    private void stopForgroundService() {
        if(serviceStarted) {
            _context.stopService(new Intent(_context, FlicBackgroundService.class));
            serviceStarted = false;
        }
    }

    public void destroy() {
        List<Flic2Button> buttons = Flic2Manager.getInstance().getButtons();

        for (Flic2Button button : Flic2Manager.getInstance().getButtons()) {
            button.removeListener(listeners.get(button.getUuid()));
            listeners.remove(button.getUuid());
        }
    }
}
