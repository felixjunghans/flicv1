package de.sdpdp.logbuch.flic_v1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.flic.poiclib.FlicButton;
import io.flic.poiclib.FlicButtonAdapter;
import io.flic.poiclib.FlicButtonListener;
import io.flic.poiclib.FlicButtonMode;
import io.flic.poiclib.FlicManager;
import io.flic.poiclib.FlicManagerAdapter;
import io.flic.poiclib.FlicManagerListener;
import io.flic.poiclib.FlicScanWizard;
import io.flutter.Log;
import io.flutter.plugin.common.MethodChannel;

public class FlicHandler {
    private String appId = "32adfdfd-7800-4b21-b2f8-d70401602b91";
    private String appSecret = "c494bc0b-734c-4526-977b-a18ad24479cf";
    private MethodChannel _channel;
    private boolean _isScanning = false;
    private HashMap<String, FlicButtonListener> listeners = new HashMap();
    private int ageBeforeIgnore = 1;
    private Boolean serviceStarted = false;
    private Context _context;
    private ButtonCallBack _buttonCallBack;

    public FlicHandler(Context context, MethodChannel channel, ButtonCallBack buttonCallBack) {
        FlicManager.init(context, appId, appSecret);
        _channel = channel;
        _context = context;
        _buttonCallBack = buttonCallBack;
        restoreButtonState();
        updateKnownButtons();
        getBluetoothState();

        FlicManager.getManager().setEventListener(new FlicManagerAdapter() {
            @Override
            public void onBluetoothStateChange(int i) {
                // set to unknown
                int convertedState = 2;

                if(i == FlicManager.BLUETOOTH_STATE_ON) {
                    convertedState = 0;
                } else if (i == FlicManager.BLUETOOTH_STATE_OFF) {
                    convertedState = 1;
                }

                invokeMethod("bluetoothStateChange", convertedState);
            }
        });
    }

    public void getBluetoothState() {
        int state = 2;
        try {
            state = FlicManager.getManager().getBluetoothState();
        } catch (Exception e) {}


        // set to unknown
        int convertedState = 2;

        if(state == FlicManager.BLUETOOTH_STATE_ON) {
            convertedState = 0;
        } else if (state == FlicManager.BLUETOOTH_STATE_OFF) {
            convertedState = 1;
        }

        invokeMethod("bluetoothStateChange", convertedState);
    }

    private Handler uiThreadHandler = new Handler(Looper.getMainLooper());

    private void restoreButtonState() {
       List<FlicButton> buttons = FlicManager.getManager().getKnownButtons();

       for (FlicButton button : FlicManager.getManager().getKnownButtons()) {
             button.connect();
             addButtonListener(button);
       }

       new android.os.Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
              getKnownButtons();
              invokeMethod("didRestore", "");
           }
       }, 10000);
    }

    private void updateKnownButtons() {
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                getKnownButtons();
            }
        },0,30000);
    }

    public void startScan() {
        Log.d("startscan", "FELIXIMUS Scanning...");
        isScanning(true);
        FlicManager.getManager().getScanWizard().start(new FlicScanWizard.Callback() {
            @Override
            public void onDiscovered(FlicScanWizard wizard, String bdAddr, int rssi, final boolean isPrivateMode, int revision) {
                if (isPrivateMode) {
                    Log.d("DiscoveredInPrivateMode", "FELIXIMUS Found a private button. Hold it down for 7 seconds to make it public." + " - " + bdAddr );
                } else {
                    Log.d("DiscoveredInNONEPrivateMode", "FELIXIMUS Now connecting to... ." + " - " + bdAddr );
                }
                invokeMethod("didDiscover", "");
            }

            @Override
            public void onBLEConnected(FlicScanWizard wizard, String bdAddr) {
                Log.d("ConnectionEstablished", "FELIXIMUS Connection established. Now verifying..." + " - " + bdAddr );
            }

            @Override
            public void onCompleted(FlicScanWizard wizard, final FlicButton button) {
                Log.d("SUCCESS", "FELIXIMUS New button successfully added!");
                addButtonListener(button);
                getKnownButtons();
                isScanning(false);
            }

            @Override
            public void onFailed(FlicScanWizard wizard, int flicScanWizardErrorCode) {
                Log.d("FAILED", "FELIXIMUS " + Integer.toString(flicScanWizardErrorCode));
                isScanning(false);
                invokeMethod("connectingFailed", String.valueOf(flicScanWizardErrorCode));
            }
        });
    }

    private void addButtonListener(FlicButton button) {
        FlicButtonListener listener = new FlicButtonAdapter() {
            @Override
            public void onConnect(FlicButton button) {
                Log.d("connect BUTTON", "FELIXIMUS BUTTON CONNECTED");
                getKnownButtons();
            }

            @Override
            public void onReady(FlicButton button) {
                    Log.d("connect BUTTON", "FELIXIMUS BUTTON READY");
                getKnownButtons();
            }

            @Override
            public void onDisconnect(FlicButton button, int flicError, boolean willReconnect) {
                    Log.d("connect BUTTON", "FELIXIMUS BUTTON DISCONNECTED");
                getKnownButtons();
            }

            @Override
            public void onConnectionFailed(FlicButton button, int status) {
                                Log.d("connect BUTTON", "FELIXIMUS BUTTON CONNECTION FAILED");
                getKnownButtons();
            }

            @Override
            public void onButtonUpOrDown(FlicButton button, boolean wasQueued, int timeDiff, boolean isUp, boolean isDown) {
                if(timeDiff <= ageBeforeIgnore) {
                    if(isUp) {
                        invokeMethod("didReceiveButtonUp", button.getButtonUuid());
                    } else if(isDown) {
                        invokeMethod("didReceiveButtonDown", button.getButtonUuid());
                    }
                }
            }

            @Override
            public void onButtonSingleOrDoubleClickOrHold(FlicButton button, boolean wasQueued, int timeDiff, boolean isSingleClick, boolean isDoubleClick, boolean isHold) {
                Log.d("Button click", "FELIXIMUS BUTTON CLICK");
                if(timeDiff <= ageBeforeIgnore) {
                    if(isSingleClick) {
                        invokeMethod("didReceiveButtonClick", button.getButtonUuid());
                    } else if(isDoubleClick) {
                        invokeMethod("didReceiveButtonDoubleClick", button.getButtonUuid());
                    } else if (isHold) {
                        invokeMethod("didReceiveButtonHold", button.getButtonUuid());
                    }
                }
            }
        };
            listeners.put(button.getButtonUuid(), listener);
            try {
                button.removeEventListener(listener);
            } catch (Exception e) {}
            button.setEventListener(listener);
            startForgroundService();
    }

    private void invokeMethod(String method, Object content) {
        uiThreadHandler.post(() -> _channel.invokeMethod(method, content));
    }

    private void removeButtonListener(FlicButton button) {
        button.removeEventListener(listeners.get(button.getButtonUuid()));
        listeners.remove(button.getButtonUuid());
    }

    public void stopScan() {
        FlicManager.getManager().getScanWizard().cancel();
        isScanning(false);
    }

    public void connect(String identifier) {
        FlicButton button = getButtonFromIdentifier(identifier);

        if(button != null) {
            button.connect();
        }
    }

    public void disconnect(String identifier) {
        FlicButton button = getButtonFromIdentifier(identifier);

        if(button != null) {
            button.disconnectOrAbortPendingConnection();
        }
    }

    public FlicButton getButtonFromIdentifier(String identifier) {
        FlicButton button = null;

        List<FlicButton> knownButtons = FlicManager.getManager().getKnownButtons();

        Log.d("knwon button size", Integer.toString(knownButtons.size()));

        for(int i = 0; i < knownButtons.size(); i++) {
           Log.d("button id", "FELIXIMUS button id: " + (knownButtons.get(i).getButtonUuid()) + " " + identifier);
           if(knownButtons.get(i).getButtonUuid().equals(identifier)) {
               button = knownButtons.get(i);
           }
        }

        return button;
    }

    public void forgetButton(String identifier) {
        FlicButton button = getButtonFromIdentifier(identifier);

        if(button != null) {
           FlicManager.getManager().forgetButton(button);
           removeButtonListener(button);
           if(listeners.size() < 1) {
               stopForgroundService();
           }
        }
        getKnownButtons();
    }

    public void indicateButtonLED(String identifier) {
            Log.d("BUTTON LED", "FELIXIMUS show LED for " + identifier);
        FlicButton button = getButtonFromIdentifier(identifier);

        if(button != null) {
              Log.d("BUTTON LED", "FELIXIMUS start LED for " + identifier);
            button.setLED(2, 1, 0, 2);
        }
    }

    public void getKnownButtons() {
        HashMap<String, HashMap> buttons = new HashMap<>();
        List<FlicButton> knownButtons = FlicManager.getManager().getKnownButtons();

        for (FlicButton button : FlicManager.getManager().getKnownButtons()) {
            HashMap newButton = new HashMap();
            int connectionState = button.getConnectionState() == 0 ? 2 : button.getConnectionState() == 2 ? 0 : button.getConnectionState();

            newButton.put("identifier", button.getButtonUuid());
            newButton.put("name", button.getName());
            newButton.put("batteryStatus", getBatteryState(button.getBatteryLevel()));
            newButton.put("batteryVoltage", 0);
            newButton.put("firmwareRevision", button.getRevision());
            newButton.put("isReady", button.isVerified());
            newButton.put("connectionState",  connectionState);
            newButton.put("version", "1");

            buttons.put(button.getButtonUuid(), newButton);
        }
        sendKnownButtons(buttons);
    }

    private int getBatteryState(Integer batteryLevel) {
        if(batteryLevel == null) {
            return 0;
        } else if(batteryLevel < 29) {
            return 1;
        } else if(batteryLevel < 49) {
            return 2;
        } else {
            return 3;
        }
    }

    public void isScanning(boolean isScanning) {
        _isScanning = isScanning;
        invokeMethod("onScanningStateChange", _isScanning);
    }

    public void sendKnownButtons(HashMap buttons) {
        uiThreadHandler.post(() -> _buttonCallBack.updateButtons(buttons, true));
    }

    public void startForgroundService() {
        if(!serviceStarted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                _context.startForegroundService(new Intent(_context, FlicBackgroundService.class));
            } else {
                _context.startService(new Intent(_context, FlicBackgroundService.class));
            }
            serviceStarted = true;
        }
    }

    public void stopForgroundService() {
        if(serviceStarted) {
            _context.stopService(new Intent(_context, FlicBackgroundService.class));
            serviceStarted = false;
        }
    }

    public void destroy() {
        List<FlicButton> buttons = FlicManager.getManager().getKnownButtons();

        for (FlicButton button : FlicManager.getManager().getKnownButtons()) {
            button.removeEventListener(listeners.get(button.getButtonUuid()));
            listeners.remove(button.getButtonUuid());
        }
    }
}
