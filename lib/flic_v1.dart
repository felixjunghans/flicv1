import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum BluetoothState { on, off, unknown }

enum FlicButtonClickType { click, double, hold }

enum FlicButtonBatteryState { unknown, low, medium, height }

enum FlicButtonConnectionState {
  connected,
  connecting,
  disconnected,
  disconnecting
}

enum FlicConnectingState { unknown, connecting, failed }

class FlicButtonClick {
  final String identifier;
  final FlicButtonClickType clickType;

  FlicButtonClick(this.identifier, this.clickType);
}

class FlicButton {
  static const String prefPrefix = "FLICV1_BUTTON_NAME_";

  final String identifier;
  final String deviceName;
  final bool isReady;
  final FlicButtonBatteryState batteryStatus;
  final int firmwareRevision;
  final bool buttonDown;
  final FlicButtonConnectionState connectionState;
  final String version;
  final bool emptyBatteryWarning;
  final VoidCallback onUpdate;
  String? customName;

  FlicButton({
    required this.identifier,
    required this.deviceName,
    required this.isReady,
    required this.batteryStatus,
    required this.firmwareRevision,
    this.buttonDown = false,
    required this.connectionState,
    required this.version,
    this.emptyBatteryWarning = false,
    required this.onUpdate,
  });

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.customName = prefs.getString("$prefPrefix$identifier");
  }

  String get name => customName ?? deviceName;

  Future<void> setButtonName(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("$prefPrefix$identifier", name);
    init();
    onUpdate();
  }

  static FlicButton fromJson(
      Map<dynamic, dynamic> json, VoidCallback onUpdate) {
    final version = json["version"] as String;
    return FlicButton(
      identifier: json["identifier"] as String,
      deviceName: json["name"] as String,
      isReady: json["isReady"] as bool,
      batteryStatus:
          FlicButtonBatteryState.values[(json["batteryStatus"] as int)],
      firmwareRevision: json["firmwareRevision"] as int,
      connectionState:
          FlicButtonConnectionState.values[(json["connectionState"] as int)],
      version: version,
      emptyBatteryWarning: version == "1"
          ? FlicButtonBatteryState.values[(json["batteryStatus"] as int)] ==
              FlicButtonBatteryState.low
          : json["batteryVoltage"] as double < 2.65,
      onUpdate: onUpdate,
    );
  }
}

class FlicV1 {
  BehaviorSubject<BluetoothState> _bluetoothState =
      BehaviorSubject<BluetoothState>.seeded(BluetoothState.unknown);
  BehaviorSubject<FlicConnectingState> _scanningState =
      BehaviorSubject<FlicConnectingState>.seeded(FlicConnectingState.unknown);
  BehaviorSubject<bool> _didRestore = BehaviorSubject<bool>.seeded(false);
  BehaviorSubject<List<FlicButton>> _connectedButtons =
      BehaviorSubject<List<FlicButton>>.seeded([]);
  BehaviorSubject<FlicButtonClick> _onFlickButtonClick =
      BehaviorSubject<FlicButtonClick>();
  BehaviorSubject<bool> _isScanning = BehaviorSubject<bool>();
  static const MethodChannel _channel = const MethodChannel('flic_v1');

  FlicV1() {
    _channel.setMethodCallHandler(handler);
    _channel.invokeMethod("getBluetoothState");
    getKnownButtons();
  }

  Stream<List<FlicButton>> get connectedButtons =>
      _connectedButtons.asBroadcastStream();

  Stream<FlicButtonClick> get onFlicButtonClick =>
      _onFlickButtonClick.asBroadcastStream();

  Stream<bool> get isScanning => _isScanning.asBroadcastStream();

  Stream<bool> get didRestore => _didRestore.asBroadcastStream();

  Stream<BluetoothState> get onBluetoothStateChanged =>
      _bluetoothState.asBroadcastStream();

  Stream<FlicConnectingState> get connectingState =>
      _scanningState.asBroadcastStream();

  Future<void> _updateButtons(List<FlicButton> buttons) async {
    for (FlicButton button in buttons) {
      await button.init();
    }
    _connectedButtons.add(buttons);
  }

  void _setButtonClicked(String identifier, bool buttonDown) {
    _updateButtons(_connectedButtons.value.map((button) {
      if (button.identifier == identifier) {
        return FlicButton(
            identifier: button.identifier,
            deviceName: button.deviceName,
            firmwareRevision: button.firmwareRevision,
            batteryStatus: button.batteryStatus,
            connectionState: button.connectionState,
            version: button.version,
            emptyBatteryWarning: button.emptyBatteryWarning,
            isReady: button.isReady,
            onUpdate: _onUpdateButton,
            buttonDown: buttonDown);
      }
      return button;
    }).toList());
  }

  void _onUpdateButton() {
    _updateButtons(_connectedButtons.value);
  }

  void _handleButtonDown(String identifier) {
    _setButtonClicked(identifier, true);
  }

  void _handleButtonUp(String identifier) {
    _setButtonClicked(identifier, false);
  }

  void _handleButtonClick(String identifier, FlicButtonClickType clickType) {
    _onFlickButtonClick.add(FlicButtonClick(identifier, clickType));
  }

  Future<dynamic> handler(MethodCall call) async {
    print(call.method);
    switch (call.method) {
      case "connectedButtons":
        try {
          final result = call.arguments as Map<dynamic, dynamic>;
          List<FlicButton> buttons = result.entries
              .map<FlicButton>((button) =>
                  FlicButton.fromJson(button.value, _onUpdateButton))
              .toList();
          _updateButtons(buttons);
        } catch (e) {
          print(e);
        }
        break;
      case "didReceiveButtonDown":
        try {
          final result = call.arguments as String;
          _handleButtonDown(result);
        } catch (e) {
          print(e);
        }
        break;
      case "didReceiveButtonUp":
        try {
          final result = call.arguments as String;
          _handleButtonUp(result);
        } catch (e) {
          print(e);
        }
        break;
      case "didReceiveButtonClick":
        try {
          final result = call.arguments as String;
          _handleButtonClick(result, FlicButtonClickType.click);
        } catch (e) {
          print(e);
        }
        break;
      case "didReceiveButtonDoubleClick":
        try {
          final result = call.arguments as String;
          _handleButtonClick(result, FlicButtonClickType.double);
        } catch (e) {
          print(e);
        }
        break;
      case "didReceiveButtonHold":
        try {
          final result = call.arguments as String;
          _handleButtonClick(result, FlicButtonClickType.hold);
        } catch (e) {
          print(e);
        }
        break;
      case "onScanningStateChange":
        try {
          final result = call.arguments as bool;
          _isScanning.add(result);
        } catch (e) {
          print(e);
        }
        break;
      case "didDiscover":
        try {
          _scanningState.add(FlicConnectingState.connecting);
        } catch (e) {
          print(e);
        }
        break;
      case "connectingFailed":
        try {
          _scanningState.add(FlicConnectingState.failed);
        } catch (e) {
          print(e);
        }
        break;
      case "didRestore":
        try {
          _didRestore.add(true);
        } catch (e) {
          print(e);
        }
        break;
      case "bluetoothStateChange":
        try {
          _didRestore.add(true);
          final result = call.arguments as int;
          _bluetoothState.add(BluetoothState.values[result]);
        } catch (e) {
          print(e);
        }
        break;
      default:
        break;
    }
  }

  Future<void> startScan() async {
    _scanningState.add(FlicConnectingState.unknown);
    await _channel.invokeMethod('startScan');
  }

  Future<void> forgetButton(String identifier, String version) async {
    _updateButtons(_connectedButtons.value
        .where((element) => element.identifier != identifier)
        .toList());
    await _channel.invokeMethod(
        'forgetButton', {"identifier": identifier, "version": version});
  }

  Future<void> indicateButtonLED(String identifier, String version) async {
    await _channel.invokeMethod(
        'indicateButtonLED', {"identifier": identifier, "version": version});
  }

  Future<void> connect(String identifier, String version) async {
    await _channel.invokeMethod(
        "connect", {"identifier": identifier, "version": version});
  }

  Future<void> getKnownButtons() async {
    await _channel.invokeMethod("getKnownButtons");
  }

  Future<void> startService() async {
    await _channel.invokeMethod("startService");
  }

  Future<void> stopService() async {
    await _channel.invokeMethod("stopService");
  }

  void dispose() {
    _connectedButtons.close();
    _onFlickButtonClick.close();
    _isScanning.close();
    _didRestore.close();
    _scanningState.close();
  }
}
