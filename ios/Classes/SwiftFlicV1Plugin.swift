import Flutter
import UIKit

public class SwiftFlicV1Plugin: NSObject, FlutterPlugin {
    let channel: FlutterMethodChannel
    let flicManager: FlicManager
    let flicManagerv2: FlicManagerV2
    var buttonsv1: [String: [String: Any]] = [:]
    var buttonsv2: [String: [String: Any]] = [:]
    
    public init(channel: FlutterMethodChannel) {
        self.channel = channel
        self.flicManager = FlicManager(channel: self.channel)
        self.flicManagerv2 = FlicManagerV2(channel: self.channel)
        super.init()
        #if targetEnvironment(simulator)
        #else
        flicManager.buttonCallback = self.updateButtons(buttons:isV1:)
        flicManagerv2.buttonCallback = self.updateButtons(buttons:isV1:)
        #endif
    }
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flic_v1", binaryMessenger: registrar.messenger())
        let instance = SwiftFlicV1Plugin(channel: channel)
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "startScan":
            self.flicManager.startScan()
            self.flicManagerv2.startScan()
            break
        case "forgetButton":
            let args = call.arguments as! Dictionary<String, String>
            let identifier: String = args["identifier"]!
            if(args["version"]! == "1") {
                self.flicManager.forgetButton(identifier: identifier)
            } else {
                self.flicManagerv2.forgetButton(identifier: identifier)
            }
            break
        case "indicateButtonLED":
            self.flicManager.indicateButtonLED(identifier: call.arguments as! String)
            break
        case "connect":
            let args = call.arguments as! Dictionary<String, String>
            let identifier: String = args["identifier"]!
            if(args["version"]! == "1") {
                self.flicManager.connect(identifier: identifier)
            } else {
                self.flicManagerv2.connect(identifier: identifier)
            }
            break
        case "disconnect":
            let args = call.arguments as! Dictionary<String, String>
            let identifier: String = args["identifier"]!
            if(args["version"]! == "1") {
                self.flicManager.disconnect(identifier: identifier)
            } else {
                self.flicManagerv2.disconnect(identifier: identifier)
            }
            break
        case "getBluetoothState":
            #if targetEnvironment(simulator)
            #else
            self.flicManager.getBluetoothState()
            #endif
          
            break
        case "getKnownButtons":
            self.flicManager.getKnownButtons()
            self.flicManagerv2.getKnownButtons()
        default:
            result(FlutterMethodNotImplemented)
            break;
        }
    }
    
    public func updateButtons(buttons: [String: [String: Any]], isV1: Bool) {
        if(isV1) {
            buttonsv1 = buttons
        } else {
            buttonsv2 = buttons
        }
        sendKnownButtons()
    }
    
    public func sendKnownButtons() {
        var buttons: [String: [String: Any]] = [:]
        buttons.merge(buttonsv1) { (current, _) -> [String : Any] in
            current
        }
        buttons.merge(buttonsv2) { (current, _) -> [String : Any] in
            current
        }
        self.channel.invokeMethod("connectedButtons", arguments: buttons);
    }
}
