//
//  FlicManager.swift
//  flic_v1
//
//  Created by Felix Junghans on 12.02.20.
//
#if targetEnvironment(simulator)

public class FlicManagerV2 {
    var stopScanTimer: Timer? = nil
    var isScanning: Bool = false
    let channel: FlutterMethodChannel
    
    public init(channel: FlutterMethodChannel) {
        self.channel = channel
    }
    
    public func startScan() {
        setIsScanning(isScanning: true)
        if #available(iOS 10.0, *) {
            stopScanTimer = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: false) { [weak self] _ in
                self?.stopScan()
            }
        }
    }
    
    public func stopScan() {
        setIsScanning(isScanning: false)
        stopScanTimer?.invalidate()
        self.getKnownButtons()
    }
    
    public func connect(identifier: String) {}
    
    public func disconnect(identifier: String) { }
    
    public func getButtonFromIdentifier(identifier: String) {}
    
    public func forgetButton(identifier: String) {}
    
    public func indicateButtonLED(identifier: String) {}
    
    @objc public func getKnownButtons() {
        var buttons: [String: [String: Any]] = [:]
        let spruehdose: [String: Any] = ["identifier": "1234", "name": "Sprühdose", "batteryStatus": 1, "firmwareRevision": "123", "isReady": true, "connectionState": 2 ]
        let clip: [String: Any] = ["identifier": "1234", "name": "Clip", "batteryStatus": 2, "firmwareRevision": "123", "isReady": true, "connectionState": 2 ]
        buttons["1234"] = spruehdose
        buttons["12345"] = clip
        
        self.sendKnownButtons(buttons: buttons)
    }
    
    public func sendKnownButtons(buttons: [String: [String: Any]]) {
        self.channel.invokeMethod("connectedButtons", arguments: buttons);
    }
    
    public func setIsScanning(isScanning: Bool) {
        self.isScanning = isScanning
        channel.invokeMethod("onScanningStateChange", arguments: isScanning)
    }
}

#else

import flic2lib
public class FlicManagerV2: NSObject, FLICManagerDelegate, FLICButtonDelegate {
    var stopScanTimer: Timer? = nil
    var isScanning: Bool = false
    let ageBeforeIgnore = 1;
    let channel: FlutterMethodChannel
    let appID: String = "f897cfe6-422f-49a6-9f5d-299d0c05ee10"
    let appSecret: String = "9cb00b2a-65b1-4db3-b495-df40bf351824"
    var buttonCallback: (([String: [String: Any]], Bool) -> ())? = nil
    
    
    public init(channel: FlutterMethodChannel) {
        self.channel = channel
        super.init()
        flic2lib.FLICManager.configure(with: self, buttonDelegate: self, background: true)
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
                self?.getKnownButtons()
            }
        }
        self.getBluetoothState()
    }
    
    public func FLICManager(_ manager: FLICManager, didChange state: FLICManagerState) {
        // set to unknown
        var convertedState = 2;
        
        if(state == FLICManagerState.poweredOn) {
            convertedState = 0;
        } else if (state == FLICManagerState.poweredOff) {
            convertedState = 1;
        }
        
        self.channel.invokeMethod("bluetoothStateChange", arguments: convertedState)
    }
    
    public func getBluetoothState() {
        let state = flic2lib.FLICManager.shared()?.state
        
        // set to unknown
        var convertedState = 2;
        
        if(state == FLICManagerState.poweredOn) {
            convertedState = 0;
        } else if (state == FLICManagerState.poweredOff) {
            convertedState = 1;
        }
        
        self.channel.invokeMethod("bluetoothStateChange", arguments: convertedState)
    }
    
    public func FLICManagerDidRestoreState(_ manager: FLICManager) {
        self.getKnownButtons()
        self.channel.invokeMethod("didRestore", arguments: "")
    }
    
    public func FLICManager(_ manager: FLICManager, didForgetButton buttonIdentifier: UUID, error: Error?) {
        self.getKnownButtons()
    }
    
    func button(_ button: FLICButton?, didUpdateBatteryVoltage voltage: Float) {
        self.getKnownButtons()
    }
    
    public func FLICManager(_ manager: FLICManager, didDiscover button: FLICButton, withRSSI RSSI: NSNumber?) {
        self.stopScan()
        button.connect()
        
        self.channel.invokeMethod("didDiscover", arguments: "")
        
        self.getKnownButtons()
    }
    
    public func FLICButtonIsReady(_ button: FLICButton) {
        self.getKnownButtons()
    }
    
    public func FLICButtonDidConnect(_ button: FLICButton) {
        self.getKnownButtons()
    }
    
    public func FLICButton(_ button: FLICButton, didChange status: FLICButtonState) {
        self.getKnownButtons()
    }
    
    public func FLICButton(_ button: FLICButton, didFailToConnectWithError error: Error?) {
        print(error?.localizedDescription ?? "")
        self.channel.invokeMethod("connectingFailed", arguments: error?.localizedDescription)
        
    }
    
    public func button(_ button: FLICButton, didReceiveButtonDown queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonDown", arguments: button.identifier.uuidString)
        }
    }
    
    public func button(_ button: FLICButton, didReceiveButtonUp queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonUp", arguments: button.identifier.uuidString)
        }
    }
    
    public func button(_ button: FLICButton, didReceiveButtonClick queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonClick", arguments: button.identifier.uuidString)
        }
    }
    
    public func button(_ button: FLICButton, didReceiveButtonDoubleClick queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonDoubleClick", arguments: button.identifier.uuidString)
        }
    }
    
    public func button(_ button: FLICButton, didReceiveButtonHold queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonHold", arguments: button.identifier.uuidString)
        }
    }
    
    public func managerDidRestoreState(_ manager: FLICManager) {
        self.getKnownButtons()
        self.channel.invokeMethod("didRestore", arguments: "")
    }
    
    public func manager(_ manager: FLICManager, didUpdate state: FLICManagerState) {
        self.getKnownButtons()
    }
    
    public func buttonDidConnect(_ button: FLICButton) {
        self.getKnownButtons()
    }
    
    public func buttonIsReady(_ button: FLICButton) {
        self.getKnownButtons()
    }
    
    public func button(_ button: FLICButton, didDisconnectWithError error: Error?) {
        self.getKnownButtons()
    }
    
    public func button(_ button: FLICButton, didFailToConnectWithError error: Error?) {
        self.getKnownButtons()
    }
    
    public func startScan() {
        print("startScan")
        flic2lib.FLICManager.shared()?.scanForButtons(stateChangeHandler: { event in
            // You can use these events to update your UI.
            switch event {
            case .discovered:
                print("A Flic was discovered.")
            case .connected:
                print("A Flic is being verified.")
            case .verified:
                print("The Flic was verified successfully.")
            case .verificationFailed:
                print("The Flic verification failed.")
            default:
                break
            }
        }) { button, error in
            if let error = error {
                print("Scanner completed with error: \(error)")
            }
            if error == nil {
                if let name = button?.name, let bluetoothAddress = button?.bluetoothAddress, let serialNumber = button?.serialNumber {
                    print("Successfully verified: \(name), \(bluetoothAddress), \(serialNumber)")
                }
                // Listen to single click only.
                button?.triggerMode = FLICButtonTriggerMode.click
            }
        }
        
        setIsScanning(isScanning: true)
        if #available(iOS 10.0, *) {
            stopScanTimer = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: false) { [weak self] _ in
                self!.stopScan()
            }
        }
    }
    
    public func stopScan() {
        flic2lib.FLICManager.shared()?.stopScan()
        setIsScanning(isScanning: false)
        stopScanTimer?.invalidate()
    }
    
    public func connect(identifier: String) {
        let button: FLICButton? = self.getButtonFromIdentifier(identifier: identifier)
        
        if(button != nil) {
            button?.connect()
        }
    }
    
    public func disconnect(identifier: String) {
        let button: FLICButton? = self.getButtonFromIdentifier(identifier: identifier)
        
        if(button != nil) {
            button?.disconnect()
        }
    }
    
    public func getButtonFromIdentifier(identifier: String) -> FLICButton? {
        return flic2lib.FLICManager.shared()?.buttons().first(where: { (arg0) -> Bool in
            return arg0.identifier.uuidString == identifier
        })
    }
    
    public func forgetButton(identifier: String) {
        print("Forget Button")
        let button: FLICButton? = self.getButtonFromIdentifier(identifier: identifier)
        
        if(button != nil) {
            flic2lib.FLICManager.shared()?.forgetButton(button!) { (id, error) in
                print("Forget Button")
            }
        }
    }
    
    // NOT SUPPORTED ANYMORE
    public func indicateButtonLED(identifier: String) {}
    
    @objc public func getKnownButtons() {
        print("get buttons")
        var buttons: [String: [String: Any]] = [:]
        
        flic2lib.FLICManager.shared()?.buttons().forEach { (value) in
            var connectionState = 0
            
            switch(value.state) {
            case .connected: connectionState = 0
                break
            case .connecting: connectionState = 1
                break
            case .disconnected: connectionState = 2
                break
            case .disconnecting: connectionState = 3
                break
            default: connectionState = 0
                break
            }
            
            let buttonData: [String: Any] = ["identifier": value.identifier.uuidString, "name": value.name as Any, "batteryStatus": 0, "firmwareRevision": value.firmwareRevision, "isReady": value.isReady, "connectionState": connectionState, "batteryVoltage": value.batteryVoltage, "version": "2" ]
            buttons[value.identifier.uuidString] = buttonData
        }
        
        print(buttons);
        self.sendKnownButtons(buttons: buttons)
    }
    
    public func sendKnownButtons(buttons: [String: [String: Any]]) {
        buttonCallback!(buttons, false)
    }
    
    public func setIsScanning(isScanning: Bool) {
        self.isScanning = isScanning
        channel.invokeMethod("onScanningStateChange", arguments: isScanning)
    }
}
#endif
