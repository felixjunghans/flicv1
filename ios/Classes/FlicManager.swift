//
//  FlicManager.swift
//  flic_v1
//
//  Created by Felix Junghans on 12.02.20.
//
#if targetEnvironment(simulator)

public class FlicManager {
      var stopScanTimer: Timer? = nil
    var isScanning: Bool = false
    let channel: FlutterMethodChannel
    
    public init(channel: FlutterMethodChannel) {
        self.channel = channel
    }
    
    public func startScan() {
        setIsScanning(isScanning: true)
        if #available(iOS 10.0, *) {
            stopScanTimer = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: false) { [weak self] _ in
                self?.stopScan()
            }
        }
    }
    
    public func stopScan() {
        setIsScanning(isScanning: false)
        stopScanTimer?.invalidate()
        self.getKnownButtons()
    }
    
    public func connect(identifier: String) {}
    
    public func disconnect(identifier: String) { }
    
    public func getButtonFromIdentifier(identifier: String) {}
    
    public func forgetButton(identifier: String) {}
    
    public func indicateButtonLED(identifier: String) {}
    
    @objc public func getKnownButtons() {
        var buttons: [String: [String: Any]] = [:]
        let spruehdose: [String: Any] = ["identifier": "1234", "name": "Sprühdose", "batteryStatus": 1, "firmwareRevision": "123", "isReady": true, "connectionState": 2 ]
        let clip: [String: Any] = ["identifier": "1234", "name": "Clip", "batteryStatus": 2, "firmwareRevision": "123", "isReady": true, "connectionState": 2 ]
        buttons["1234"] = spruehdose
        buttons["12345"] = clip
        
        self.sendKnownButtons(buttons: buttons)
    }
    
    public func sendKnownButtons(buttons: [String: [String: Any]]) {
        self.channel.invokeMethod("connectedButtons", arguments: buttons);
    }
    
    public func setIsScanning(isScanning: Bool) {
        self.isScanning = isScanning
        channel.invokeMethod("onScanningStateChange", arguments: isScanning)
    }
}

#else

import fliclib
public class FlicManager: NSObject, SCLFlicManagerDelegate, SCLFlicButtonDelegate {
    var stopScanTimer: Timer? = nil
    var isScanning: Bool = false
    let ageBeforeIgnore = 1;
    let channel: FlutterMethodChannel
    let appID: String = "f897cfe6-422f-49a6-9f5d-299d0c05ee10"
    let appSecret: String = "9cb00b2a-65b1-4db3-b495-df40bf351824"
    var buttonCallback: (([String: [String: Any]], Bool) -> ())? = nil
    
    
    public init(channel: FlutterMethodChannel) {
        self.channel = channel
        super.init()
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
                self?.getKnownButtons()
            }
        }
        SCLFlicManager.configure(with: self, defaultButtonDelegate: self, appID: appID, appSecret: appSecret, backgroundExecution: true)
        self.getBluetoothState()
    }
    
    public func flicManager(_ manager: SCLFlicManager, didChange state: SCLFlicManagerBluetoothState) {
        // set to unknown
        var convertedState = 2;
        
        if(state == SCLFlicManagerBluetoothState.poweredOn) {
            convertedState = 0;
        } else if (state == SCLFlicManagerBluetoothState.poweredOff) {
            convertedState = 1;
        }
        
        self.channel.invokeMethod("bluetoothStateChange", arguments: convertedState)
    }
    
    public func getBluetoothState() {
        let state = SCLFlicManager.shared()?.bluetoothState
        
        // set to unknown
        var convertedState = 2;
        
        if(state == SCLFlicManagerBluetoothState.poweredOn) {
            convertedState = 0;
        } else if (state == SCLFlicManagerBluetoothState.poweredOff) {
            convertedState = 1;
        }
        
        self.channel.invokeMethod("bluetoothStateChange", arguments: convertedState)
    }
    
    public func flicManagerDidRestoreState(_ manager: SCLFlicManager) {
        self.getKnownButtons()
        self.channel.invokeMethod("didRestore", arguments: "")
    }
    
    public func flicManager(_ manager: SCLFlicManager, didForgetButton buttonIdentifier: UUID, error: Error?) {
        self.getKnownButtons()
    }
    
    public func flicManager(_ manager: SCLFlicManager, didDiscover button: SCLFlicButton, withRSSI RSSI: NSNumber?) {
        self.stopScan()
        button.connect()
        
        self.channel.invokeMethod("didDiscover", arguments: "")
        
        self.getKnownButtons()
    }
    
    public func flicButtonIsReady(_ button: SCLFlicButton) {
        self.getKnownButtons()
    }
    
    public func flicButtonDidConnect(_ button: SCLFlicButton) {
        self.getKnownButtons()
    }
    
    public func flicButton(_ button: SCLFlicButton, didChange status: SCLFlicButtonBatteryStatus) {
        self.getKnownButtons()
    }
    
    public func flicButton(_ button: SCLFlicButton, didFailToConnectWithError error: Error?) {
        print(error?.localizedDescription ?? "")
        self.channel.invokeMethod("connectingFailed", arguments: error?.localizedDescription)
        
    }
    
    public func flicButton(_ button: SCLFlicButton, didReceiveButtonDown queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonDown", arguments: button.buttonIdentifier.uuidString)
        }
    }
    
    public func flicButton(_ button: SCLFlicButton, didReceiveButtonUp queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonUp", arguments: button.buttonIdentifier.uuidString)
        }
    }
    
    public func flicButton(_ button: SCLFlicButton, didReceiveButtonClick queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonClick", arguments: button.buttonIdentifier.uuidString)
        }
    }
    
    public func flicButton(_ button: SCLFlicButton, didReceiveButtonDoubleClick queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonDoubleClick", arguments: button.buttonIdentifier.uuidString)
        }
    }
    
    public func flicButton(_ button: SCLFlicButton, didReceiveButtonHold queued: Bool, age: Int) {
        if (age <= ageBeforeIgnore) {
            self.channel.invokeMethod("didReceiveButtonHold", arguments: button.buttonIdentifier.uuidString)
        }
    }
    
    public func startScan() {
        SCLFlicManager.shared()?.startScan()
        setIsScanning(isScanning: true)
        if #available(iOS 10.0, *) {
            stopScanTimer = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: false) { [weak self] _ in
                self!.stopScan()
            }
        }
    }
    
    public func stopScan() {
        SCLFlicManager.shared()?.stopScan()
        setIsScanning(isScanning: false)
        stopScanTimer?.invalidate()
    }
    
    public func connect(identifier: String) {
        let button: SCLFlicButton? = self.getButtonFromIdentifier(identifier: identifier)
        
        if(button != nil) {
            button?.connect()
        }
    }
    
    public func disconnect(identifier: String) {
        let button: SCLFlicButton? = self.getButtonFromIdentifier(identifier: identifier)
        
        if(button != nil) {
            button?.disconnect()
        }
    }
    
    public func getButtonFromIdentifier(identifier: String) -> SCLFlicButton? {
        return SCLFlicManager.shared()?.knownButtons().first(where: { (arg0) -> Bool in
            let (_, value) = arg0
            return value.buttonIdentifier.uuidString == identifier
        })?.value
    }
    
    public func forgetButton(identifier: String) {
        print("Forget Button")
        let button: SCLFlicButton? = self.getButtonFromIdentifier(identifier: identifier)
        
        if(button != nil) {
            SCLFlicManager.shared()?.forget((button)!)
        }
    }
    
    public func indicateButtonLED(identifier: String) {
        let button: SCLFlicButton? = self.getButtonFromIdentifier(identifier: identifier)
        
        if(button != nil) {
            button?.indicateLED(SCLFlicButtonLEDIndicateCount.count3);
        }
    }
    
    @objc public func getKnownButtons() {
        print("get buttons")
        var buttons: [String: [String: Any]] = [:]
        
        SCLFlicManager.shared()?.knownButtons().forEach({ (arg0) in
            let (key, value) = arg0
            let buttonData: [String: Any] = ["identifier": value.buttonIdentifier.uuidString, "name": value.name, "batteryStatus": value.batteryStatus.rawValue, "firmwareRevision": value.firmwareRevision(), "isReady": value.isReady, "connectionState": value.connectionState.rawValue, "batteryVoltage": 0, "version": "1" ]
            buttons[key.uuidString] = buttonData
        })
        
        print(buttons);
        self.sendKnownButtons(buttons: buttons)
    }
    
    public func sendKnownButtons(buttons: [String: [String: Any]]) {
        buttonCallback!(buttons, true)
    }
    
    public func setIsScanning(isScanning: Bool) {
        self.isScanning = isScanning
        channel.invokeMethod("onScanningStateChange", arguments: isScanning)
    }
}
#endif
