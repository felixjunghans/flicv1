#import "FlicV1Plugin.h"
#if __has_include(<flic_v1/flic_v1-Swift.h>)
#import <flic_v1/flic_v1-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flic_v1-Swift.h"
#endif

@implementation FlicV1Plugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlicV1Plugin registerWithRegistrar:registrar];
}
@end
